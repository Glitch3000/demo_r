# demo_r



## Problem Statement

RKVST Test Engineer Coding Test
This is a link to the public API documentation for the RKVST SaaS product.
https://docs.rkvst.com/docs/api-reference/

We would like you to define and write some tests for the Assets / Events REST APIs.
The candidate will be provided with a JWT bearer access token for authenticated access to a test instance of the Archivist SaaS. An example of
accessing the Assets and Events APIs for this test instance would be
curl -fsS -H "Authorization: Bearer ${TOKEN}" -X GET https://app.soak.wild.jitsuin.io/archivist/v2/assets
curl -fsS -H "Authorization: Bearer ${TOKEN}" -X GET https://app.soak.wild.jitsuin.io/archivist/v2/assets/-/events

The intent is to provide RKVST with a detailed view of a candidate's testing strategy decisions and also design and implementation skills for test
cases. It is hoped that a candidate has the facilities to build and run the tests they create, but if they have not we will make appropriate
allowances.

Please do not implement stress tests or load the system heavily, this is a shared environment. You should limit yourself to creating at most a few
10s of Assets and a few 100s of Events.

The implementation technology and language are up to the candidate and should be chosen so that they feel they can do the best job, but use of
technologies appropriate to the RKVST system is preferred.

If there is a deficiency in these requirements and / or the online documentation then candidates are encouraged to point it out and/or remedy it. It
is not expected that the test implementation will need to be much longer than a few hundred lines (depending on code density), we do not expect
full coverage of the API surface. If it does, reduce the requirements appropriately, stating what the resulting limitations are. Or, likewise, if you
are limited by time then please reduce the coverage and again state what you did not attempt to achieve with your solution. 

We do expect the candidate to justify their decisions on which areas of the API surface to focus on.
Obviously the more complete / polished a candidates solution is, the more RKVST can determine a candidates depth and breadth of knowledge.

Please provide a “Readme” with your submission with instructions on how to run the tests.
Include in the “Readme” a succinct description of what you did. What tests did you intend to write and why. What problems you overcame in
getting them to run. Which tests could not be executed. Treat it as a memory aid for a conversation during the next phase of interview.

## Setup

```

Install Python 3.10
python -m pip install -r ./requirements.txt


(If run on windows ensure robot is added to pythonpath)
(Execution was only performed on a linux machine)

```


## Execution

```
cd {this location}
robot --pythonpath ./  ./smoke.robot     

```


## Test Rationale

Defined/implicit Constraints on testing
- Only system testing can be performed as there is no access to allow for integration and component tests
- Stress & load testing ruled out due to environment nature
- Extensive access permissions testing cannot be done as I do not have knowledge of way to generate nuanced tokens. However simply no token, gibberish token and token provided will be examined
- Further access permissions policies should be tested timeouts, ip address sources, etc
- Unable to use telemetry to target smoke testing on frequently used commands
- Unable to use user/customer feedback to determine critical functionality to drive testing
- Unable to execute non-functional performance testing without an understanding over acceptable response times
- Testing focusing on full requests for smoke tests 
- Unable to test for certain response codes GET {206(too many asset -stress testing-), 401 (user not authenticated on the system difference to authorized?), 429(exceed rate limit for requests -stress testing-), default(unknown error?)} 
- Unable to test for certain response codes POST {206(too many asset -stress testing-), 401 (user not authenticated on the system difference to authorized?),  429(exceed rate limit for requests -stress testing-), default(unknown error?)} 
- Out side of scope testing blobs being referenced within assets

# Smoke tests
Attempting to exercise the system through some simple and standard uses of the API. 
- Keep execution under 60 seconds
- Standard Uses GET & POST for both Assets and Attached Events
- Access Permissions Tests


# To Do 
(Public API) - Need to set asset flag to public then use this api to check if link returned?

(Open API) - Quick test manually via postman to get schema to api_schema.json a method it could be used to automate check portions of the command responses however given that the api for this system is used by others it probably is adventageous to test the system with static examples as they will depend on its stability regardless of changes.

(Scenario Tests)
Fictional meat packing business, load assets and log events from a "simulated" point of view
"Interupted" requests for a posted asset/event
Possibly test assets with referenced blobs


# Notes
- Asset example includes attachment perhaps be better to have a stand alone example with this example being later
- 402 Error code on POST  /archivist/v2/assets 
- Not a big deal response codes no listed for events POST API 403 (for example)

- Black used to more rigidly enforce PEP8/coding standard in python sections