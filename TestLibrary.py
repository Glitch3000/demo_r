""" Python for doing the heavy lifting of the tests """

import json
from types import SimpleNamespace
import time
import requests


class TestLibrary:
    """This is a user written keyword library."""

    ROBOT_LIBRARY_SCOPE = "SUITE"

    @staticmethod
    def store_in_file(content):
        """Useful for debug"""
        unix_timestamp = str(time.time()).replace(".", "_")

        with open(f"{unix_timestamp}.tmp", "w", encoding="utf8") as file:
            file.write(content.decode("utf-8"))

    @staticmethod
    def fetch_token(token_type):
        """Read Stored Access tokens"""
        # In more compelte example possible use a method for generating these
        with open(
            f"./access_tokens/{token_type}.auth.token", "r", encoding="utf8"
        ) as file:
            token = file.readline()
        return token

    @staticmethod
    def create_public_asset_v2():
        """Create Asset"""
        asset = {
            "behaviours": ["RecordEvidence", "Attachments"],
            "proof_mechanism": "SIMPLE_HASH",
            "public": False,
        }
        return asset

    @staticmethod
    def post_asset_request(url, token, asset):
        """Simple POST request for an asset with token"""
        headers = {"Authorization": f"Bearer {token}"}
        response = requests.post(url, headers=headers, json=asset)
        return response

    @staticmethod
    def post_asset_request_no_auth(url, asset):
        """Simple POST request for an asset with token"""
        response = requests.post(url, json=asset)
        return response

    @staticmethod
    def post_event_request(base_url, uuid, token):
        """Simple POST request for an asset with token"""
        event = {
            "operation": "Record",
            "behaviour": "RecordEvidence",
            "event_attributes": {
                "arc_display_type": "Safety Conformance",
                "Safety Rating": "90",
                "inspector": "spacetime",
            },
            "timestamp_declared": "2019-11-27T14:44:19Z",
            "principal_declared": {
                "issuer": "idp.synsation.io/1234",
                "subject": "phil.b",
                "email": "phil.b@synsation.io",
            },
        }
        url = base_url + "/" + uuid + "/events"
        headers = {"Authorization": f"Bearer {token}"}
        response = requests.post(url, headers=headers, json=event)
        return response

    @staticmethod
    def return_uuid(response):
        """Pull out UUID in response"""
        payload = response.content.decode("utf-8")
        json_schema = json.loads(payload, object_hook=lambda d: SimpleNamespace(**d))
        identity = json_schema.identity.split("/")
        uuid = identity[1]
        return uuid

    @staticmethod
    def num_of_events_correct(content, num_of_events):
        """Count number events in json fed in"""
        json_data = json.loads(content)
        counted_events = len(json_data["events"])

        if counted_events == int(num_of_events):
            return True

        return False

    @staticmethod
    def return_event_uuid(response):
        """Full out UUID in response"""
        payload = response.content.decode("utf-8")
        json_schema = json.loads(payload, object_hook=lambda d: SimpleNamespace(**d))
        identity = json_schema.identity.split("/")
        event_uuid = identity[3]
        return event_uuid

    @staticmethod
    def get_all_request(url, token):
        """Simple GET request for all assets with token"""
        headers = {"Authorization": f"Bearer {token}"}
        response = requests.get(url, headers=headers)
        return response

    @staticmethod
    def get_all_request_no_auth(url):
        """Simple GET request for all assets without attempting to authenticate"""
        response = requests.get(url)
        return response

    @staticmethod
    def get_asset_events_request(base_url, uuid, token):
        """Fetch all events logged against uuid asset"""
        url = base_url + "/" + uuid + "/events"
        headers = {"Authorization": f"Bearer {token}"}
        response = requests.get(url, headers=headers)
        return response

    @staticmethod
    def get_asset_individual_event_request(base_url, uuid, token, event_uuid):
        """Fetch all events logged against uuid asset"""
        url = base_url + "/" + uuid + "/events/" + event_uuid
        headers = {"Authorization": f"Bearer {token}"}
        response = requests.get(url, headers=headers)
        return response

    @staticmethod
    def get_request(base_url, uuid, token):
        """Simple GET request for an asset with token"""
        url = base_url + "/" + uuid
        headers = {"Authorization": f"Bearer {token}"}
        response = requests.get(url, headers=headers)
        return response

    @staticmethod
    def get_request_no_auth(base_url, uuid):
        """Prod URL without attempting to authenticate"""
        url = base_url + "/" + uuid
        return requests.get(url)

    @staticmethod
    def return_content(response):
        """Extract json content"""
        return response.content

    @staticmethod
    def return_status_code(response):
        """Extract status code"""
        return response.status_code

    @staticmethod
    def verify_invalid_response(response_content):
        """Verify that the response reads as invalid token"""
        json_data = json.loads(response_content)
        if json_data["message"] == "Invalid Token":
            return True
        return False

    @staticmethod
    def verify_no_autho_response(response_content):
        """Verify that the response reads as no authorization header"""
        json_data = json.loads(response_content)
        if json_data["message"] == "Missing Authorization Header":
            return True

        return False

    @staticmethod
    def verify_asset_v2(response_content, uuid, public_mode):
        """Verify all fields on asset are present and correct
        It could be considered better to split out all the checks
        for debug logs and quick tracability of faults however this
        approach can helpful for future automation

        This could be expanded for more fields along with the asset
        creation to more paramertized

        """

        json_data = json.loads(response_content)  # Convert json to dictionary

        expected_entries = ["identity"]
        expected_entries.append("behaviours")
        expected_entries.append("confirmation_status")
        expected_entries.append("attributes")
        expected_entries.append("owner")
        expected_entries.append("proof_mechanism")
        expected_entries.append("public")
        expected_entries.append("tenant_identity")
        expected_entries.append("tracked")

        valid = True
        err_msg = ""

        try:
            # Check for potential errors
            for entry in expected_entries:
                if entry not in json_data:
                    err_msg += f"::{entry} missing from response::"
                    valid = False
                elif entry == "identity":
                    if json_data["identity"] != f"assets/{uuid}":
                        value = json_data["identity"]
                        err_msg += f"::identity value not valid - {value}::"
                        valid = False
                elif entry == "proof_mechanism":
                    if json_data["proof_mechanism"] != "SIMPLE_HASH":
                        value = json_data["proof_mechanism"]
                        err_msg += f"::Proof Mechanism value not valid - {value}::"
                        valid = False
                elif entry == "public":
                    if json_data["public"] is not public_mode:
                        value = json_data["public"]
                        err_msg += f"::Public value not valid - {value}::"
                        valid = False

        except Exception as fault:
            valid = False
            err_msg += f"::{fault}::"

        err_msg = bytes(err_msg, "utf-8")

        return valid, err_msg

    @staticmethod
    def verify_event_v2(response_content, uuid, event_uuid):
        """Verify event needs completing current just checking
        identity and for presense of files currently
        TBD

        """

        json_data = json.loads(response_content)  # Convert json to dictionary

        # These could be read in from a file schema
        expected_entries = ["identity"]
        expected_entries.append("event_attributes")
        expected_entries.append("asset_attributes")
        expected_entries.append("behaviour")
        expected_entries.append("timestamp_declared")
        expected_entries.append("timestamp_accepted")
        expected_entries.append("timestamp_committed")
        expected_entries.append("principal_declared")
        expected_entries.append("principal_accepted")

        expected_entries.append("confirmation_status")
        expected_entries.append("transaction_id")
        expected_entries.append("block_number")
        expected_entries.append("transaction_index")
        expected_entries.append("from")
        expected_entries.append("tenant_identity")

        valid = True
        err_msg = ""

        try:
            # Check for potential errors
            for entry in expected_entries:
                if entry not in json_data:
                    err_msg += f"::{entry} missing from response::"
                    valid = False
                elif entry == "identity":
                    if json_data["identity"] != f"assets/{uuid}/events/{event_uuid}":
                        value = json_data["identity"]
                        err_msg += f"::identity value not valid - {value}::"
                        valid = False

        except Exception as fault:
            valid = False
            err_msg += f"::{fault}::"

        err_msg = bytes(err_msg, "utf-8")

        return valid, err_msg
