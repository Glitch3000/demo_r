*** Settings ***
Documentation     Smoke Suite - Sanity Testing Simple Behaviours
...
...               Keywords are imported from the resource file
Resource          keywords.resource
Library           DateTime
Library           Collections

*** Variable ***
# ${UUID}   /6c37ab6f-6100-4188-af08-26403cc90069


*** Test Cases ***
GET /v2/assets - Valid Token Test Case
    [Documentation]    Get All Case

    ${token_value}=    Fetch Bearer Token    valid
    ${response}=    Get All Assets    ${token_value}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200
    # If I had more control I would setup a fresh instance store ten assets then check
    # only these ten are reported back

GET /v2/assets - Invalid Token Test Case
    [Documentation]    Expected Failure

    ${token_value}=    Fetch Bearer Token    invalid
    ${response}=    Get All Assets    ${token_value}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 403 # Expected access forbidden
    ${content}=    Extract Content    ${response}
    ${valid}=    Verify Invalid Asset Response    ${content}
    Should Be True    ${valid}

GET /v2/assets - Blank Token Test Case
    [Documentation]    Expected Failure

    ${token_value}=    Fetch Bearer Token    blank
    ${response}=    Get All Assets    ${token_value}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 403 # Expected access forbidden
    ${content}=    Extract Content    ${response}
    ${valid}=    Verify Invalid Asset Response    ${content}
    Should Be True    ${valid}

GET /v2/assets - No Authentication Test Case
    [Documentation]    Expected Failure

    ${response}=    Get All Assets No Authentication
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 403 # Expected access forbidden
    ${content}=    Extract Content    ${response}
    ${valid}=    Verify No Autho Asset Response    ${content}
    Should Be True    ${valid}

POST /v2/assets & GET /v2/assets/{uuid} Valid Token Test Case
    [Documentation]    POST then GET Success

    ${token_value}=    Fetch Bearer Token    valid
    ${public_mode}    Set Variable    ${False}

    ${asset}=    Create Public Asset
    ${response}=    Post Asset    ${token_value}    ${asset}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200 # Success
    ${UUID}=    Extract UUID    ${response}
    ${content}=    Extract Content    ${response}
    ${valid}     ${err_msg}=    Verify Asset    ${content}    ${UUID}    ${public_mode}
    Should Be True    ${valid}

    ${response}=    Get Asset    ${UUID}    ${token_value}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200 # Success

    ${content}=    Extract Content    ${response}
    ${valid}     ${err_msg}=    Verify Asset    ${content}    ${UUID}    ${public_mode}
    Should Be True    ${valid}


POST /v2/assets - Invalid Token Test Case
    [Documentation]    Expected Failure

    ${token_value}=    Fetch Bearer Token    invalid
    ${asset}=    Create Public Asset
    ${response}=    Post Asset    ${token_value}    ${asset}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 403 # Expected access forbidden

    ${content}=    Extract Content    ${response}
    ${valid}=    Verify Invalid Asset Response    ${content}
    Should Be True    ${valid}

    # Should search that the post didn't add anything, how?
    # Ideally use a clean environment and check there are no assets created

POST /v2/assets - Blank Token Test Case
    [Documentation]    Expected Failure

    ${token_value}=    Fetch Bearer Token    blank
    ${asset}=    Create Public Asset
    ${response}=    Post Asset    ${token_value}    ${asset}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 403 # Expected access forbidden

    ${content}=    Extract Content    ${response}
    ${valid}=    Verify Invalid Asset Response    ${content}
    Should Be True    ${valid}

POST /v2/assets - No Authentication Test Case
    [Documentation]    Expected Failure

    ${asset}=    Create Public Asset
    ${response}=    Post Asset No Authentication    ${asset}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 403 # Expected access forbidden

    ${content}=    Extract Content    ${response}
    ${valid}=    Verify No Autho Asset Response    ${content}
    Should Be True    ${valid}


POST /v2/assets Valid & GET /v2/assets/{uuid} Invalid Token Test Case
    [Documentation]    POST Successfully GET Fails

    ${token_value}=    Fetch Bearer Token    valid
    ${public_mode}    Set Variable    ${False}
    ${asset}=    Create Public Asset
    ${response}=    Post Asset    ${token_value}    ${asset}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200 # Success
    ${UUID}=    Extract UUID    ${response}
    ${content}=    Extract Content    ${response}
    ${valid}     ${err_msg}=    Verify Asset    ${content}    ${UUID}    ${public_mode}
    Should Be True    ${valid}

    ${token_value}=    Fetch Bearer Token    invalid
    ${response}=    Get Asset    ${UUID}    ${token_value}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 403 # Expected access forbidden

    ${content}=    Extract Content    ${response}
    ${valid}=    Verify Invalid Asset Response    ${content}
    Should Be True    ${valid}

POST /v2/assets Valid & GET /v2/assets/{uuid} Blank Token Test Case
    [Documentation]    POST Successfully Ensure Fetch Fails with blank Token

    ${token_value}=    Fetch Bearer Token    valid
    ${public_mode}    Set Variable    ${False}
    ${asset}=    Create Public Asset
    ${response}=    Post Asset    ${token_value}    ${asset}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200 # Success
    ${UUID}=    Extract UUID    ${response}
    ${content}=    Extract Content    ${response}
    ${valid}     ${err_msg}=    Verify Asset    ${content}    ${UUID}    ${public_mode}
    Should Be True    ${valid}

    ${token_value}=    Fetch Bearer Token    blank
    ${response}=    Get Asset    ${UUID}    ${token_value}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 403 # Expected access forbidden
    ${content}=    Extract Content    ${response}
    ${valid}=    Verify Invalid Asset Response    ${content}
    Should Be True    ${valid}

POST /v2/assets Valid & GET /v2/assets/{uuid} No Authentication Test Case
    [Documentation]    POST Successfully Ensure Fetch Fails with no authentication

    ${token_value}=    Fetch Bearer Token    valid
    ${public_mode}    Set Variable    ${False}
    ${asset}=    Create Public Asset
    ${response}=    Post Asset    ${token_value}    ${asset}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200 # Success
    ${UUID}=    Extract UUID    ${response}
    ${content}=    Extract Content    ${response}
    ${valid}     ${err_msg}=    Verify Asset    ${content}    ${UUID}    ${public_mode}
    Should Be True    ${valid}

    ${response}=    Get Asset No Authentication    ${UUID}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 403 # Expected access forbidden
    ${content}=    Extract Content    ${response}
    ${valid}=    Verify No Autho Asset Response    ${content}
    Should Be True    ${valid}


#
# Public Asset API test should go here
#
#



Event Post to Asset and Retrieve Each Even Then All Events Happy Day Case
    [Documentation]    POST /v2/assets and POST /v2/{uuid}/Event Then GET /v2/{uuid}/Event/
    ${num_of_events}    Set Variable    10

    ${token_value}=    Fetch Bearer Token    valid
    ${public_mode}    Set Variable    ${False}
    ${asset}=    Create Public Asset
    ${response}=    Post Asset    ${token_value}    ${asset}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200 # Success

    ${UUID}=    Extract UUID    ${response}

    ${content}=    Extract Content    ${response}
    ${valid}     ${err_msg}=    Verify Asset    ${content}    ${UUID}    ${public_mode}
    Should Be True    ${valid}

    # Give some time for the asset to be commited to the blockchain
    # This could be requirement driven?
    Sleep    3s

    @{list_of_event_UUIDs}=    Create List

    FOR     ${index}    IN RANGE    1   ${num_of_events}
        ${response}=    Post Event    ${UUID}    ${token_value}
        ${status_code}=    Extract Status Code    ${response}
        Should Be True  ${status_code} == 200 # Success
        ${content}=    Extract Content    ${response}
        ${event_UUID}=    Extract Event UUID    ${response}
        Append To List    ${list_of_event_UUIDs}    ${event_UUID}
    END

    FOR     ${event_UUID}    IN     @{list_of_event_UUIDs}
        ${response}=    Get Asset Individual Event  ${UUID}    ${token_value}    ${event_UUID}
        ${status_code}=    Extract Status Code    ${response}
        Should Be True  ${status_code} == 200 # Success
        ${event_UUID_response}=    Extract Event UUID    ${response}
        ${content}=    Extract Content    ${response}
        ${valid}     ${err_msg}=    Verify Event    ${content}    ${UUID}    ${event_UUID}
        Should Be True    ${valid}
        Should be equal as strings    ${event_UUID}    ${event_UUID_response}
    END

    # Check fetch all events stored
    ${response}=    Get Asset Events  ${UUID}    ${token_value}
    ${content}=    Extract Content    ${response}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200 # Success
    ${state}=     Number Of Events Correct    ${content}    ${num_of_events}
    # TBD Add more to validate event json returned for all the events
    Should Be True    ${state}

Event Post to Asset and Retrieve Each Even Then All Events Ensure Single Invalid Event Ignored Case
    [Documentation]    Create Public Asset and POST Event to it ensure created

    ${num_of_events}    Set Variable    10
    ${num_of_invalid_events}    Set Variable    2

    ${token_value}=    Fetch Bearer Token    valid
    ${public_mode}    Set Variable    ${False}
    ${asset}=    Create Public Asset
    ${response}=    Post Asset    ${token_value}    ${asset}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200 # Success

    ${UUID}=    Extract UUID    ${response}

    ${content}=    Extract Content    ${response}
    ${valid}     ${err_msg}=    Verify Asset    ${content}    ${UUID}    ${public_mode}
    Should Be True    ${valid}

    # Give some time for the asset to be commited to the blockchain
    # This could be requirement driven?
    Sleep    3s

    @{list_of_event_UUIDs}=    Create List

    FOR     ${index}    IN RANGE    1   ${num_of_events}
        ${response}=    Post Event    ${UUID}    ${token_value}
        ${status_code}=    Extract Status Code    ${response}
        Should Be True  ${status_code} == 200 # Success
        ${content}=    Extract Content    ${response}
        ${event_UUID}=    Extract Event UUID    ${response}
        Append To List    ${list_of_event_UUIDs}    ${event_UUID}
    END

    # Invalid Event Posts
    ${invalid_token_value}=    Fetch Bearer Token    invalid
    FOR     ${index}    IN RANGE    1   ${num_of_invalid_events}

        ${response}=    Post Event    ${UUID}    ${invalid_token_value}
        ${status_code}=    Extract Status Code    ${response}
        Should Be True  ${status_code} == 403 # Expected Failures
    END


    FOR     ${event_UUID}    IN     @{list_of_event_UUIDs}
        ${response}=    Get Asset Individual Event  ${UUID}    ${token_value}    ${event_UUID}
        ${status_code}=    Extract Status Code    ${response}
        Should Be True  ${status_code} == 200 # Success
        ${event_UUID_response}=    Extract Event UUID    ${response}
        ${content}=    Extract Content    ${response}
        ${valid}     ${err_msg}=    Verify Event    ${content}    ${UUID}    ${event_UUID}
        Should Be True    ${valid}
        Should be equal as strings    ${event_UUID}    ${event_UUID_response}
    END

    # Check fetch all events stored
    ${response}=    Get Asset Events  ${UUID}    ${token_value}
    ${content}=    Extract Content    ${response}
    ${status_code}=    Extract Status Code    ${response}
    Should Be True  ${status_code} == 200 # Success
    ${state}=     Number Of Events Correct    ${content}    ${num_of_events}
    # TBD Add more to validate event json returned for all the events
    Should Be True    ${state}
